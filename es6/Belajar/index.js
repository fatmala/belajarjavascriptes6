//Untuk mendeklrasikan sebuah variabel ada 3 cara yaitu menggunakan const, let, dan var.
//var(function scope) & let(block scope)
function looping(){
  for (var i=0; i<5; i++){
    console.log(i);
  }
  function inLooping(){
    console.log(i);
  }
  inLooping();
}

console.log(looping());
//console.log(i);

var kode = [1, 2, 3, 4];

function appendKode (users){
  kode =users.map(function (user){
    return user.code;
  });
}

appendKode([
  {
    code : 10
  }
]);

kode;


//let x = 1; //only declare one scope function
//var x = 2;
//x; 

for (let i=0; i<5; i++){
  console.log(i);
}

if (true){
  let i = "100";
  console.log("i =  ",i);
}

//console.log(i);

//belajar
const numberss = [1, 5, 7, 3];

function getMax (arr) {
  let max = -Infinity;
  arr.forEach(function (number) {
    max = Math.max(max, number);
  });
  return max;
}

const max = getMax(numberss);

max;

//sama seperti let namun nilainya tidak dapat diubah (immutable variables)
// => arrow function itu hanyalah penyederhanaan penulisan sebuah function. untuk lebih jelasnya kalian bisa mencoba code arrow-1.js di bawah ini. Tapi bukan hanya untuk penyederhanaan saja, memang ada saatnya dimana kita benar-benar harus memakai arrow function.
//const phi = 3.14;
//phi = 3.147;

//console.log(phi);

//contoh 1 (arrow function)
const func1 = function(a,b) {
  return a + b;
};
console.log(func1(3, 3));

//contoh 2
const func2 = (a, b) => {
  return a + b;
};
console.log(func2(3, 3));

//contoh 3
const func3 = (a, b) => a + b;
console.log(func3(3, 3));

//contoh 4
const func4 = (dobel) => dobel + dobel;
console.log(func4(5));

//contoh 5
const func5 = dobel => dobel + dobel;
console.log(func5(5));

//contoh 6
const numbers = [1,2,3,4,5];
const kalidua1 = numbers.map(function(number){
  return 2 * number;
});
console.log(kalidua1);

//contoh 7 
const kalidua2 = numbers.map(number => 2 * number);
console.log(kalidua2);

//const
const contoh = [5,6,7,8];
function appendCodes (users){
    users.forEach((user) => {
        return contoh.push(user.code);
    });
}

appendCodes([
    {
        code : 10
    }
]);

console.log(contoh);


//Default Parameters Value1
function buy (item, amount = 1) {
  amount;
  item;
  /*if (amount === undefined) {
    console.log('here');
    amount = 1;
  }*/

  return;
}

buy('shoess');

//Default Parameters Value2
function createA(){
  return 5;
}

function add(a = createA(), b = a*2, c = b+3) {
  return a + b + c;
}

console.log(add())
console.log(add(2))
console.log(add(2,3))
console.log(add(2,4,6))

//Rest parameters
function multiply(number, ...nums){
  return nums.map(function(value) {
    return number * value
  });
}

console.log(multiply(5, 6, 7));

//spread parameters
const arr1 = [1,2,3,4,5];
const arr2 = ["a, b, c"];
const combineArr = [...arr1,...arr2];
console.log(combineArr);

//spread 2
function mySum(num1, num2, num3){
  console.log(num1 + num2 + num3);
}

let params = [43, 23, 55];
mySum(...params);

//Template literals1 (multi line)
//normal string
var pesan1 = 'nama saya\n' +
'adalah\n' +
'venata'
console.log(pesan1)
console.log('string text line 1\n' + 'string text line 2');
//use template literals
var pesan = `nama saya 
adalah 
venata`
console.log(pesan)
console.log('string text line 1 string text line 2');

//Template literals2
//normal string
var a = 5;
var b = 10;
console.log('Fifteen is ' + (a+b) + ' and\nnot ' + (2*a+b) + '.');
//use tempalate string
var c = 4;
var d = 6;
console.log(`Fifteen is ${a+b} and not ${2 * a + b}.`);

//extra example
const activities =[
  'running',
  'swimming',
  'Eating'
];
const message = `My name is Jeon and I enjoy ${activities.join(' ,')}`
console.log(message);

//tag 

const myTag = (literals, name) => {
  console.log('Literals', literals); 
  console.log('Interpolation', name); 

  return 'Result from myTag';
};

const name = 'Steve';
const result = myTag `Hello ${name}!`;

console.log(result); 

//raw 
function tag(strings, ...values){
  console.log(strings.raw[0]);
}
const message2 = tag`hello my name\nis Bob, and my age is\n 10`

//property shorthand
//normal
var firstname = 'daniel';
var lastname = 'felton';
var age = 34;

var person ={
  firstname: firstname,
  lastname: lastname,
  age : age
}
 person;

 //es6
var firstname = 'daniel';
var lastname = 'felton';
var age = 34;

var person ={
  firstname,
  lastname,
  age
}
 person;

 //computed property names
 //general
 var eatables = {vegetable: 'Carrot'}
 var fruit_var = 'fruit'
 eatables[fruit_var] = 'Apple'
 eatables;
 //es6
 var fruit_var = 'fruit'
 var eatables = {[fruit_var]: 'Apple', [fruit_var + ' Cake']: 'yummy', vegetable: 'Carrot'}
 eatables;

 //method properties
 //general
 var kids = {
   names: 'ray',
   age: 8,
   haveABirthday: function () {
     this.age++;
   }
 }
 kids.haveABirthday();
 kids;
 //es6
 var kids = {
  names: 'ray',
  age: 8,
  haveABirthday () {
    this.age++;
  }
}
kids.haveABirthday();
kids;

//modules
/*import {add2} from './add';
const value  = add2(1, 2);
console.log(value);*/

//classes
//extends
class Person {
  constructor (name, age){
    this.name = name;
    this.age = age;
  }
  jump (){
    console.log(jump)
  }
}

class Employee extends Person {
  constructor (name, age, years){
    super(name, age);
    this.years = years;
  }
  quit () {
    console.log('I quit my job');
    this.years = 0;
  }
  jump () { console.log('jump really high');}
}

const employee = new Employee('vivin', 21, 3);
employee;
employee.jump();
employee.quit();

//super
class Person2 {
  constructor (name, age){
    this.name = name;
    this.age = age;
  }
  getName (){
    return this.name;
  }
}

class Employee2 extends Person2 {
  constructor (name2, age2, years2){
    super(name2, age2);
    this.years2 = years2;
  }
  quit () {
    console.log('I quit my job');
    this.years2 = 0;
  }
  getName (){
    return super.getName() + '!'
  }
}

const employee2 = new Employee2('vivin', 21, 3);
const name2 = employee2.getName();
name2;
 
//static members
class People {
  constructor (nama, umur){
    this.nama = nama;
    this.umur = umur;
  }

  static setName (people, nama){
    people.nama = nama;
  }
}

const people = new People('bob', 20);
People.setName(people, 'hello');
people;

//get and set
class Orang {
  constructor(namee){
    this._namee = namee;
  }

  get namee(){
    return this._namee.toUpperCase();
  }

  set namee(newName){
    this._namee = newName;
  }

  walk(){
    console.log(this._namee + ' is walking');
  }
}

let bob = new Orang('vivin');
console.log(bob.namee);   

//array mathcing
function getConfig(){
  return [
    true,
    10,
    3,
    4,
    6
  ]
}
const [isOn, amount, ...rest] = getConfig();
isOn;
amount;
rest;

//object matching ?? 
var config = {};

function setConfig({
  isOn,
  amount
}) {
  config = {
      isOn,
      amount
    }
  }
  setConfig({
    isOn: false,
    amount: 20
  });
  config;

  //Deep object matching
  function getConfig2(){
    return {
      isOn2: true,
      amount2: 10,
      servers: {
        a: true,
        b: false
      }
    }
  }

  var {
    isOn2: mySpecialIsBool
  } = getConfig2();

  mySpecialIsBool;

  //Default value
  //const arra = [1];
  const arra = [1, 'contoh'];

  var [amount3, b = 100] = arra;

  amount3;
  b;
