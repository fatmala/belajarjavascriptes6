//sama seperti let namun nilainya tidak dapat diubah (immutable variables)
// => arrow function itu hanyalah penyederhanaan penulisan sebuah function. untuk lebih jelasnya kalian bisa mencoba code arrow-1.js di bawah ini. Tapi bukan hanya untuk penyederhanaan saja, memang ada saatnya dimana kita benar-benar harus memakai arrow function.
//const phi = 3.14;
//phi = 3.147;

//console.log(phi);

//contoh 1 (arrow function)
const func1 = function(a,b) {
  return a + b;
};
console.log(func1(3, 3));

//contoh 2
const func2 = (a, b) => {
  return a + b;
};
console.log(func2(3, 3));

//contoh 3
const func3 = (a, b) => a + b;
console.log(func3(3, 3));

//contoh 4
const func4 = (dobel) => dobel + dobel;
console.log(func4(5));

//contoh 5
const func5 = dobel => dobel + dobel;
console.log(func5(5));

//contoh 6
const numbers = [1,2,3,4,5];
const kalidua1 = numbers.map(function(number){
  return 2 * number;
});
console.log(kalidua1);
