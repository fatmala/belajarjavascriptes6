//Untuk mendeklrasikan sebuah variabel ada 3 cara yaitu menggunakan const, let, dan var.
//var(function scope) & let(block scope)
function looping(){
  for (var i=0; i<5; i++){
    console.log(i);
  }
  function inLooping(){
    console.log(i);
  }
  inLooping();
}

looping();
//console.log(i);
